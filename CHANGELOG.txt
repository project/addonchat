$Id

  ===========
  PRE-RELEASE
  ===========

    03/02/2010 (Modifications per Drupal Maintainer Request)

      * Drupal Coding Standards Compliance
        - Fixed all script files to ensure they end with a single \n
        - Array block indentation fixed
        - Removed trailing \n after function declarations
        - Removed various extraneous \n's in functions
        - switch/case statement indentation fixed
        - Multi-lined string indentation cleaned up
        - UNIX EOL (\n) conversion fixed in README.txt, INSTALL.txt, and
          addonchat_exit.php
        - Fixed all documents to wrap (when possible) at 80 characters to fall
          in line with Drupal array span standard. While this is not required
          of Drupal coding standards, it seems appropriate.
        - Verified that no lines end with unnecessary white space

      * User interface strings now in sentence case

      * Module no longer writes directly to the role or permission
        tables. INSTALL.txt has been updated. The original intent was to
        automatically set up and configure pre-defined recommended roles
        and assign recommended permissions to newly created and pre-existing
        roles based on their existing permission set. This functionality is
        not necessary.

      * Module now removes all installed Drupal variables via variable_del
