<?php

/**
 * @file
 * AddonChat Remote Authentication Script
 *
 * This script authenticates a user using Drupal's permission system.
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

  $CALLER = "Drupal 6";

  require_once('./includes/bootstrap.inc');
  require_once('./modules/user/user.module');
  header("Content-type: text/plain");

  drupal_bootstrap(DRUPAL_BOOTSTRAP_LATE_PAGE_CACHE);
  variable_set("addonchat_access", time());

  $name = isset($_REQUEST['username']) ? $_REQUEST['username'] : "";
  $pass = isset($_REQUEST['password']) ? $_REQUEST['password'] : "";
  $rasv = isset($_REQUEST['rasver']) ? intval($_REQUEST['rasver']) : 20;

  $ras_version = ($rasv > 20) ? "2.1" :
    variable_get("addonchat_ras_version", "2.0");

  echo "scras.version = $ras_version\n";

  /*
    Load User
    Replace with user_load_by_name with Drupal 7
  */
  if (!($user = user_load(array('name' => $name)))) {
    /* User does not exist, use 'anonymous user' permissions */
    $user_perms = _addonchat_get_permissions_by_role('anonymous user');

    if ($user_perms === FALSE) {
      echo "drupal.auth.message = Failed to load for 'anonymous user'\n";
      echo "user.usergroup.id = 0\n";
      echo "user.usergroup.can_login = 0\n";
    }
    else {
      echo "user.uid = 0\n";

      if ($ras_version == "2.0") {
        echo "user.usergroup.id = " .
          (int)_addonchat_gid_hash($user_perms) . "\n";
      }
      else {
        echo "user.usergroup.id = 0\n";
      }

      echo "user.usergroup.level = " . _addonchat_get_level($user_perms) . "\n";
      echo "user.usergroup.icon = " . _addonchat_get_icon($user_perms) . "\n";

      foreach ($user_perms as $permname => $permvalue)
        echo "user.usergroup.$permname = $permvalue\n";

      _addonchat_display_misc_settings();
    }

    echo "drupal.auth.role = anonymous user\n";
    echo "drupal.auth.caller = $CALLER\n";

    if ($user_perms['can_login'] == 1) {
      db_query("REPLACE INTO {addonchat_wc} (username, uid, admin) VALUES " .
        "('%s', 0, %d)", $name, $user_perms['is_admin']);
   }

    exit;
  }

  /*
    Verify Password, allowing both plain text & md5 encoded passwords
    Case sensitive
  */
  if (strcmp($user->pass, $pass) && strcmp($user->pass, md5($pass))) {
    echo "user.usergroup.id = 0\n";
    echo "user.usergroup.can_login = 0\n";
    echo "drupal.auth.message = Invalid Password\n";
    echo "drupal.auth.caller = $CALLER\n";
    exit;
  }

  /* Authenticated User */
  echo "user.uid = " . $user->uid . "\n";

  /* Let's first set privileges for 'authenticated user' */
  $user_perms = _addonchat_get_permissions_by_role('authenticated user');

  /* Now we grab a list of all of the user's roles */
  $role_id_array = _addonchat_get_roles_by_uid($user->uid);
  echo "drupal.auth.roles = (" . implode(", ", $role_id_array) . ")\n";
  foreach ($role_id_array as $rid)
    $user_perms = _addonchat_perm_merge($user_perms,
      _addonchat_get_permissions_by_role_id($rid));

  db_query("REPLACE INTO {addonchat_wc} (username, uid, admin) VALUES " .
    "('%s', %d, %d)", $user->name, $user->uid, $user_perms['is_admin']);

  if ($ras_version == "2.0") {
    echo "user.usergroup.id = " . (int)_addonchat_gid_hash($user_perms) . "\n";
  }
  else {
    echo "user.usergroup.id = 0\n";
  }

  echo "user.usergroup.level = " . _addonchat_get_level($user_perms) . "\n";
  echo "user.usergroup.icon = " . _addonchat_get_icon($user_perms) . "\n";

  /* Display the permissions */
  foreach ($user_perms as $permname => $permvalue)
    echo "user.usergroup.$permname = $permvalue\n";

  /* Misc Settings */
  _addonchat_display_misc_settings();

  /* Debug Data */
  echo "drupal.auth.role = authenticated user\n";
  echo "drupal.auth.caller = $CALLER\n";

  exit;

  /**
   * Merge AddonChat Permission Array
   */
  function _addonchat_perm_merge($a1, $a2) {
    $retval = array();

    foreach ($a1 as $key => $value) {
      $retval[$key] = $value;
      if ($a2[$key] > $value) {
        $retval[$key] = $a2[$key];
      }
    }

    return $retval;
  }

  /**
   * Hash usergroup ID by permission set
   * Function is not necessary with RAS 2.1
   */
  function _addonchat_gid_hash($permissions) {
    $retval = (float) 0.0;

    $i = 0;
    foreach ($permissions as $perm_name => $perm_value) {
      $retval += (float) pow(2, (float) intval($perm_value) * (float) $i);
      $i++;
    }

    $retval++;

    if ($retval > (pow(2, 32) -1)) {
      echo "drupal.auth.ugid = $retval\n";
      $retval = (int)$retval;
      if ($retval == 0) {
        $retval = 1;
      }
      else if ($retval < 0) {
        $retval *= -1;
      }
    }

    return (int)$retval;
  }

  /**
   * Retrieve Roles by UID
   */
  function _addonchat_get_roles_by_uid($uid) {
    $retval = array();

    $r1 = db_query("SELECT ur.rid FROM {users_roles} ur WHERE ur.uid=%d", $uid);
    while ($user_role_map = db_fetch_object($r1))
      $retval[] = $user_role_map->rid;

    return $retval;
  }

  /**
   * Get permissions by role id
   */
  function _addonchat_get_permissions_by_role_id($role_id) {
    $r2 = db_query("SELECT p.pid, p.perm FROM {permission} p WHERE p.rid=%d",
      $role_id);

    if ($perm = db_fetch_object($r2)) {
      /* Load Permission Map */
      $permap = _addonchat_permission_map();

      /* Extract permissions and trim */
      $permission_array = explode(",", $perm->perm);
      foreach ($permission_array as $pid => $permname)
        $permission_array[$pid] = trim($permname);

      $retval = array();
      foreach ($permap as $addonchat_perm_name => $drupal_perm_name) {
        if (in_array($drupal_perm_name, $permission_array)) {
          $retval[$addonchat_perm_name] = 1;
        }
        else {
          $retval[$addonchat_perm_name] = 0;
        }
      }

      /* Return permission array */
      return $retval;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Get Permissions By Role
   */
  function _addonchat_get_permissions_by_role($role_name) {
    $r1 = db_query("SELECT r.rid FROM {role} r WHERE r.name = '%s'",
      $role_name);

    if ($role = db_fetch_object($r1)) {
      return _addonchat_get_permissions_by_role_id($role->rid);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Set Rank/Level based on user privileges
   */
  function _addonchat_get_level($p) {
    $level = 1;

    if ($p['can_login'] == 1) {
      $level = 2;
    }
    if ($p['is_speaker'] == 1) {
      $level = 3;
    }
    if ($p['is_super_moderator'] == 1) {
      $level = 4;
    }
    if ($p['is_admin'] == 1) {
      $level = 5;
      if ($p['can_affect_admin'] == 1) {
        $level = 6;
      }
    }

    return $level;
  }

  /**
   * Set Icon based on user privileges
   */
  function _addonchat_get_icon($p) {
    $icon = 0;

    if ($p['is_speaker'] == 1) {
      $icon = 1;
    }
    if ($p['is_super_moderator'] == 1) {
      $icon = 3;
    }
    if ($p['is_admin'] == 1) {
      $icon = 2;
    }

    return $icon;
  }

  /**
   * Set miscellaneous authentication settings
   */
  function _addonchat_display_misc_settings() {
    $misc_settings = array(
      'post_delay'      => 0,
      'max_msg_length'  => 1024,
    );

    foreach ($misc_settings as $name => $default)
      print "user.usergroup." . $name . " = " . variable_get('addonchat_' .
        $name, $default) . "\n";
  }

  /**
   * Map AddonChat Permissions to Drupal Permissions
   */
  function _addonchat_permission_map() {
    $permap = array(
      'can_login'             => 'can login',
      'allow_pm'              => 'allow private msg',
      'allow_room_create'     => 'create rooms',
      'idle_kick'             => 'kick if idle',
      'is_admin'              => 'chat administrator',
      'is_speaker'            => 'guest speaker',
      'is_super_moderator'    => 'chat moderator',
      'can_kick'              => 'can kick users',
      'can_affect_admin'      => 'can affect admins',
      'can_grant'             => 'can grant admins',
      'can_cloak'             => 'can cloak',
      'can_see_cloak'         => 'can see cloaked',
      'can_ban'               => 'can ban',
      'can_ban_subnet'        => 'can ban subnets',
      'can_system_speak'      => 'can system speak',
      'can_enable_moderation' => 'enable moderation',
      'can_silence'           => 'can silence',
      'allow_bbcode'          => 'allow bbcode',
      'allow_html'            => 'allow html',
      'allow_color'           => 'allow color',
      'msg_scroll'            => 'enable scrollback',
      'can_fnick'             => 'can fnick',
      'can_nick'              => 'can change nick',
      'filter_shout'          => 'filter shouting',
      'filter_profanity'      => 'enable text filter',
      'filter_word_replace'   => 'enable text replacement',
      'can_launch_website'    => 'can launch website',
      'allow_admin_console'   => 'allow RAC access',
      'can_view_transcripts'  => 'view RAC transcripts',
      'can_msg'               => 'can send messages',
      'can_action'            => 'can send actions',
      'can_transfer'          => 'can transfer users',
      'can_join_nopw'         => 'join room without pass',
      'can_topic'             => 'can set topic',
      'can_close'             => 'can close room',
      'is_unmoderated'        => 'is not moderated',
      'can_ipquery'           => 'can query ip',
      'can_geo_locate'        => 'can geolocate',
      'can_query_ether'       => 'can query ether',
      'can_clear_screen'      => 'can clear screens',
      'can_clear_history'     => 'can clear history',
      'allow_img_tag'         => 'allow img tag',
      'login_cloaked'         => 'login cloaked',
      'allow_avatars'         => 'allow avatars',
      'can_random'            => 'allow random',
    );

    /* Add 'ac.' prefix to drupal permissions to minimize conflict */
    foreach ($permap as $acperm => $drupalperm)
      $permap[$acperm] = 'ac.' . $drupalperm;

    return $permap;
  }

?>
