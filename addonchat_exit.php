<?php

/**
 * @file
 * AddonChat Exit URL. Called (optionally) when user exits chat room.
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

  require_once('./includes/bootstrap.inc');
  require_once('./modules/user/user.module');
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  variable_set("addonchat_access", time());

  db_query("DELETE FROM {addonchat_wc} WHERE username='%s'", $user->name);

  if (isset($_REQUEST['close'])) {
  ?>
    <html><body>
      <script
        type="text/javascript"
        language="javascript">window.close();
      </script>
    </body></html>
  <?php
  }
  else {
    header('location: ' . $base_url . '/');
  }
  exit(0);
?>
