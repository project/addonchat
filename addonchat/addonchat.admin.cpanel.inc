<?php

/**
 * @file
 * AddonChat Module: Admin/Account Control Panel Link
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

/**
 * AddonChat Account Control Panel Link Page Form
 * Administer: Site Configuration: AddonChat: Account Control Panel
 */
function addonchat_admin_cpanel() {
  $form = array();

  $cpanel_link = variable_get("addonchat_cpanel_link", "http://www.addonchat.com/login.html");

  $form['instructions'] = array(
    '#type' => 'markup',
    '#value' => '<div style="padding-top: 6px;">' .
      t('<a href="' . $cpanel_link .
      '" target="addonchatcp" onClick="window.open(\'' . $cpanel_link .
      '\', \'addonchatcp\', \'width=790,height=565,status=no,scrollbars=yes' .
      ',menubar=no,resizable=yes\'); return false">Click here</a> to launch your
      AddonChat account control panel.') . '</div>',
  );

  return $form;
}
