<?php

/**
 * @file
 * AddonChat Module: Admin/Integration
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

/**
 * AddonChat Account Integration Settings Form
 */
function addonchat_admin_integration() {
  $form = array();

  /* Automatic Login */
  $form['autologin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic login'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['autologin']['addonchat_autologin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatic user login'),
    '#description' => t('Click this checkbox to enable automatic login for
      authenticated users.'),
    '#default_value' => variable_get('addonchat_autologin', 1),
    '#required' => TRUE,
  );

  /* Height - Embedded */
  $form['embed_dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Height (embedded)'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['embed_dimensions']['addonchat_embed_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat room height (in page)'),
    '#description' => t('Enter the height (in pixels) of your chat room as
      shown within a Drupal page.'),
    '#default_value' => variable_get('addonchat_embed_height', 450),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE,
  );

  /* Width, Height - Popup Window */
  $form['popup_dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Width &amp; height (popup window)'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['popup_dimensions']['addonchat_popup_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat room width (popup)'),
    '#description' => t('Enter the width (in pixels) of your chat room as
      shown within a popup window.'),
    '#default_value' => variable_get('addonchat_popup_width', 640),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE,
  );

  $form['popup_dimensions']['addonchat_popup_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat room height (popup)'),
    '#description' => t('Enter the height (in pixels) of your chat room as
      shown within a popup window.'),
    '#default_value' => variable_get('addonchat_popup_height', 480),
    '#size' => 6,
    '#maxlength' => 4,
    '#required' => TRUE,
  );

  /* Exit URLs */
  $form['exiturl'] = array(
    '#type' => 'fieldset',
    '#title' => t('Exit URLs'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['exiturl']['addonchat_exiturl_embedded_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable exit URL (embedded chat room / block)'),
    '#default_value' => variable_get('addonchat_exiturl_embedded_enabled', 1),
    '#description' => t("<p>When enabled, the web site specified below will be
      launched when a user logs out of your chat room when used within a
      Drupal block (embedded). The default link to 'addonchat_exit.php' is
      recommended as it will automatically redirect the user to your drupal
      base URL and update the who's chatting system.</p>"),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#required' => TRUE,
  );

  $form['exiturl']['addonchat_exiturl_embedded'] = array(
    '#type' => 'textfield',
    '#title' => t('Exit URL (embedded chat room / block)'),
    '#default_value' =>
      variable_get('addonchat_exiturl_embedded', _addonchat_default_exiturl()),
    '#size' => 64,
    '#maxlength' => 256,
    '#description' => t("Please enter the web site to be launched when a user
      logs out of your embedded/block chat room."),
    '#required' => FALSE,
  );

  $form['exiturl']['addonchat_exiturl_popup_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Enable exit URL (popup link)'),
    '#default_value' => variable_get('addonchat_exiturl_popup_enabled', 1),
    '#description' => t("<p>When enabled, the web site specified below will be
      launched when a user logs out of your chat room when used within a popup
      window. The default link to 'addonchat_exit.php' is recommended as it
      will automatically close the window and update the who's chatting
      system.</p>"),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#required' => TRUE,
  );

  $form['exiturl']['addonchat_exiturl_popup'] = array(
    '#type' => 'textfield',
    '#title' => t('Exit URL (popup link)'),
    '#default_value' => variable_get('addonchat_exiturl_popup',
      _addonchat_default_exiturl(TRUE)),
    '#size' => 64,
    '#maxlength' => 256,
    '#description' => t("Please enter the web site to be launched when a user
      logs out of your chat room when it is being used in a popup window. The
      default setting is recommended."),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Construct Default Remote Authentication URL
 */
function _addonchat_default_exiturl($close = FALSE) {
  global $base_url;

  $rasurl = trim($base_url);
  if (!(drupal_substr($rasurl, -1) == '/')) {
    $rasurl .= '/';
  }
  $rasurl .= 'addonchat_exit.php';

  if ($close) {
    $rasurl .= '?close';
  }

  return $rasurl;
}
