<?php

/**
 * @file
 * AddonChat Module: Admin/Advanced
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

/**
 *  AddonChat Account Integration Settings Form
 * Administer: Site Configuration: AddonChat: Advanced Settings
 */
function addonchat_advanced_integration() {
  $form = array();

  /*
    Custom Parameters (Text Field)
  */
  $form['custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('AddonChat applet parameters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['custom']['instructions'] = array(
    '#type' => 'markup',
    '#value' =>
      t("Advanced users may add additional AddonChat applet parameters here:"),
  );

  $form['custom']['addonchat_applet_params'] = array(
    '#type' => 'textarea',
    '#title' => 'AddonChat applet parameters',
    '#required' => FALSE,
    '#rows' => 15,
    '#attributes' => array('style' => 'font-family: Courier', 'wrap' => 'off'),
    '#default_value' => variable_get('addonchat_applet_params',
      "<param name=\"centerimage\" value=\"true\">\n" .
      "<param name=\"boxborder\" value=\"false\">\n\n" .
      "<!-- Uncomment the following after v8.5 is released. -->\n" .
      "<!--param name=\"image\" value=\"loader.gif\"-->\n\n" .
      "<!-- " .
      "The following options should only be modified by advanced users. " .
      "-->\n" .
      "<param name=\"username.prompt.readonly\" value=\"false\">\n" .
      "<param name=\"password.prompt.readonly\" value=\"false\">\n" .
      "<param name=\"password.prompt.after.initial\" value=\"false\">\n" .
      "<param name=\"login.prompt.after.autologin\" value=\"false\">\n" .
      "<param name=\"subroom.single\" value=\"false\">\n" .
      "<param name=\"rndname\" value=\"false\">\n"
    ),
  );

  /* Custom Parameters (RAS) */
  $form['custom_ras'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced remote authentication settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['custom_ras']['instructions'] = array(
    '#type' => 'markup',
    '#value' => t("Advanced users may add additional output to their remote
      authentication script here:"),
  );

  $form['custom_ras']['addonchat_ras_params'] = array(
    '#type' => 'textarea',
    '#title' => 'AddonChat remote authentication system parameters',
    '#required' => FALSE,
    '#attributes' => array('style' => 'font-family: Courier', 'wrap' => 'off'),
    '#default_value' => variable_get('addonchat_ras_params',
      "drupal.auth.comment = custom parameters"),
  );

  return system_settings_form($form);
}

/**
 * Unused.
 */
function addonchat_advanced_integration_validate($form, &$form_state) {
}
