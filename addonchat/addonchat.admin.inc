<?php

/**
 * @file
 * AddonChat Module: Admin
 */

/**
 * http://www.addoninteractive.com
 * support@addoninteractive.com
 */

/**
 * AddonChat Account Setup Form
 */
function addonchat_admin() {
  $form = array();

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic account information'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['account']['instructions'] = array(
    '#type' => 'markup',
    '#value' => t("Please enter your <a href=\"http://www.addonchat.com\"
      target=\"addonchat\">AddonChat</a> account details. After making changes
      here, you may click the <b>Lookup account settings</b> button to
      automatically load your account details (server, port, language, etc.)."),
  );

  $form['account']['addonchat_accountid'] = array(
    '#type' => 'textfield',
    '#title' => t('Account ID'),
    '#default_value' => variable_get('addonchat_accountid', 'SC-'),
    '#size' => 13,
    '#maxlength' => 11,
    '#description' => t("Please enter your AddonChat account ID
      (e.g., SC-12345)"),
    '#required' => TRUE,
  );

  $form['account']['addonchat_password'] = array(
    '#type' => 'password',
    '#title' => t('Account password'),
    '#default_value' => variable_get('addonchat_password', ''),
    '#size' => 32,
    '#maxlength' => 32,
    '#description' => ((variable_get('addonchat_password', '') == '') ?
      t("Please enter your AddonChat account password.") :
      t("Leave blank if you do not need to update password.")),
    '#required' => FALSE,
  );

  $form['account']['addonchat_rasurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote authentication URL'),
    '#default_value' =>
      variable_get('addonchat_rasurl', _addonchat_default_rasurl()),
    '#size' => 64,
    '#maxlength' => 256,
    '#description' => t("Please enter the authentication script you wish to use.
      The default setting is recommended."),
    '#required' => FALSE,
  );

  $form['account']['addonchat_enable_ras'] = array(
    '#type' => 'radios',
    '#title' => t('Enable Drupal user authentication'),
    '#default_value' => variable_get('addonchat_enable_ras', 1),
    '#description' => t("<p>When enabled, user access to your AddonChat chat
      room will be managed by your Drupal user permissions. You may then modify
      chat room permissions from the Permissions section of the User management
      menu. For this feature to work, you must have purchased the remote
      authentication module, or have an AddonChat Professional PLUS or
      Enterprise account. You must click the <b>Lookup account settings</b>
      button below to activate remote authentication through Drupal.</p>"),
    '#options' => array(0 => t('No'), 1 => t('Yes')),
    '#required' => TRUE,
  );

  $form['account']['lookup'] = array(
    '#type'   => 'submit',
    '#value'  => t('Lookup account settings'),
    '#submit' => array(
      'system_settings_form_submit',
      'addonchat_account_lookup'
     ),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['settings']['instructions2'] = array(
    '#type' => 'markup',
    '#value' => t("<p>Use this form to modify your AddonChat account settings.
      You should use the <b>Lookup account settings</b> button (above) to
      automatically retrieve your account details settings if possible.</p>"),
    '#weight' => 0,
  );

  $server_name_options = array(
    'betaclient.addonchat.com' => 'betaclient.addonchat.com'
   );
  for ($i = 0; $i <= 32; $i++)
    $server_name_options['client' . $i . '.addonchat.com'] =
      'client' . $i . '.addonchat.com';

  $server_port_options = array(8080 => 'TCP 8080');
  for ($j = 8000; $j <= 8009; $j++)
    $server_port_options[$j] = "TCP $j";

  $form['settings']['addonchat_server_name'] = array(
    '#type' => 'select',
    '#title' => t('Assigned chat server'),
    '#description' => t('Please select your assigned server. If automatic lookup
      fails, this can be found in your chat room link code (codebase) in your
      AddonChat account control panel'),
    '#default_value' =>
      variable_get('addonchat_server_name', 'client0.addonchat.com'),
    '#options' => $server_name_options,
    '#required' => TRUE,
    '#weight' => 1,
  );

  $form['settings']['addonchat_server_port'] = array(
    '#type' => 'select',
    '#title' => t('Assigned chat server port'),
    '#description' => t('Please select your assigned chat server port. If
      automatic lookup fails, this can be found in the Settings -&gt; Advanced
      section of your AddonChat account control panel.'),
    '#default_value' => variable_get('addonchat_server_port', '8000'),
    '#options' => $server_port_options,
    '#required' => TRUE,
    '#weight' => 2,
  );

  $language_options = array(
    "ar"     => "Arabic / Arabe (Traditional, Persian Gulf)",
    "bg"     => "Bulgarian / Bulgare (Bulgaria)",
    "cz"     => "Czech / Tch&#232;que (Czech Republic)",
    "da"     => "Danish / Danois (Kingdom of Denmark)",
    "nl"     => "Dutch / N&#233;erlandais (Netherlands; Formal)",
    "nl_i"   => "Dutch / N&#233;erlandais (Netherlands; Informal)",
    "en_uk"  => "English / Anglais (United Kingdom)",
    "en"     => "English / Anglais (United States)",
    "fr"     => "French / Fran&#231;ais (France; Universal)",
    "de"     => "German / Allemand (Germany; Formal)",
    "de_i"   => "German / Allemand (Germany; Informal)",
    "fi"     => "Finnish / Finnois",
    "he"     => "Hebrew / H&#233;breu (Israel)",
    "hu"     => "Hungarian / Hongrois (Hungary)",
    "it"     => "Italian / Italien (Italy)",
    "jp"     => "Japanese / Japonais (Japan)",
    "pl"     => "Polish / Polonais (Poland)",
    "pt_br"  => "Portuguese / Portugais (Brazil)",
    "ru"     => "Russian / Russe (Russian Federation)",
    "sl"     => "Slovenian / Slov&#232;ne (Slovenia)",
    "es_pe"  => "Spanish; Castellano / Espagnol; Castillan (Peru)",
    "es_es"  => "Spanish / Espagnol (Spain; Universal)",
    "sv"     => "Swedish / Su&#233;dois (Sweden)",
    "tr"     => "Turkish / Turc (Republic of Turkey) [1]",
    "tr2"    => "Turkish / Turc (Republic of Turkey) [2]",
  );

  if (function_exists("html_entity_decode")) {
    foreach ($language_options as $lcode => $lname) {
      $language_options[$lcode] =
        html_entity_decode($lname, ENT_COMPAT, "UTF-8");
    }
  }
  elseif (function_exists("mb_convert_encoding")) {
    foreach ($language_options as $lcode => $lname) {
      $language_options[$lcode] =
        mb_convert_encoding($lname, "UTF-8", "HTML-ENTITIES");
    }
  }

  $form['settings']['addonchat_lang'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => variable_get('addonchat_lang', 'en'),
    '#options' => $language_options,
    '#required' => TRUE,
    '#weight' => 3,
  );

  return system_settings_form($form);
}

/**
 * Validate AddonChat Administration Form
 */
function addonchat_admin_validate($form, &$form_state) {
  $account_id = $form_state['values']['addonchat_accountid'];
  $account_password = $form_state['values']['addonchat_password'];

  /* No password set, so we need to request it... */
  if (
    (variable_get('addonchat_password', '') == '') &&
    ($account_password == '')) {
    form_set_error('addonchat_password',
      t("Please enter your AddonChat customer account password."));
  }

  /* User does not wish to update password */
  elseif ($account_password == '') {
    $form_state['values']['addonchat_password'] =
      variable_get('addonchat_password', '');
  }

  /*
    User is changing (or setting for the first time) a password
    MD5 encode it.
  */
  else {
    $form_state['values']['addonchat_password'] =
      md5($form_state['values']['addonchat_password']);
  }

  $form_state['values']['addonchat_server_port'] =
    intval($form_state['values']['addonchat_server_port']);
  $form_state['values']['addonchat_enable_ras'] =
    intval($form_state['values']['addonchat_enable_ras']);

  $account_id = preg_replace('/\D+/', '', $account_id);
  $form_state['values']['addonchat_accountid'] = $account_id;

  if (!is_numeric($account_id)) {
    form_set_error('addonchat_accountid',
      t('Please enter a valid AddonChat account ID.'));
  }
}

/**
 * Automatic Account Lookup & Remote Authentication Setup
 */
function addonchat_account_lookup() {
  $qra = _addonchat_fetch_account(variable_get('addonchat_accountid', 0),
    variable_get('addonchat_password', ''));

  if ($qra === FALSE) {
    drupal_set_message(
      t('<div style="margin-left: 12px;">Automatic lookup failed.<br /> Please
        enter your account settings (server, port, etc..) manually.</div>'),
        'error', FALSE);
    return;
  }

  if ($qra['edition'] == -1) {
    drupal_set_message(
      t('<div style="margin-left: 12px;">Automatic Lookup Error: <b>' .
      filter_xss($qra['ras_capable']) . '</b></div>'), 'error', FALSE);
    return;
  }

  variable_set('addonchat_edition',     intval($qra['edition']));
  variable_set('addonchat_ras_capable', intval($qra['ras_capable']));
  variable_set('addonchat_server_name', trim($qra['server_name']));
  variable_set('addonchat_server_port', intval($qra['server_port']));
  variable_set('addonchat_cpanel_link', trim($qra['cpanel_login']));

  drupal_set_message(
    t('Automatic account lookup successful. Account settings imported.'));

  if (
    (intval($qra['ras_capable']) == 1) &&
    (intval(variable_get('addonchat_enable_ras', 0)) == 1)) {
    _addonchat_account_set_ras();
  }
  elseif (
    (intval($qra['ras_capable']) == 1) &&
    (intval(variable_get('addonchat_enable_ras', 0) == 0))) {
    _addonchat_account_unset_ras();
  }
  elseif (
    (intval($qra['ras_capable']) == 0) &&
    (intval(variable_get('addonchat_enable_ras', 0) == 1))) {
    variable_set('addonchat_enable_ras', 0);
    drupal_set_message(t('<div style="margin-left: 12px;">Your AddonChat
      account does not support remote authentication.</div>'),
      'warning', FALSE);
  }
  else {
    variable_set('addonchat_enable_ras', 0);
  }
}


/**
 * Set Remote Authentication Script (Form Control)
 */
function _addonchat_account_set_ras() {
  $qra = _addonchat_enable_remote_authentication(
    variable_get('addonchat_accountid', 0),
    variable_get('addonchat_password', ''),
    variable_get('addonchat_rasurl', '')
  );

  if ($qra === FALSE) {
    drupal_set_message(t('<div style="margin-left: 12px;">Error setting
      remote authentication URL.<br />Please update your control panel manually
      to enable remote authentication.</div>'), 'error', FALSE);
    variable_set('addonchat_enable_ras', 0);
    return;
  }

  if ($qra['result'] == 0) {
    drupal_set_message(t('<div style="margin-left: 12px;">
      Error setting remote authentication URL: <b>' .
      filter_xss($qra['message']) . '</b></div>'), 'error', FALSE);
    variable_set('addonchat_enable_ras', 0);
    return;
  }

  drupal_set_message(t('Remote authentication set and enabled.'));
}

/**
 * Unset Remote Authentication Script (Form Control)
 */
function _addonchat_account_unset_ras() {
  $qra = _addonchat_disable_remote_authentication(
    variable_get('addonchat_accountid', 0),
    variable_get('addonchat_password', '')
  );

  if ($qra === FALSE) {
    drupal_set_message(
      t('<div style="margin-left: 12px;">Error disabling remote authentication
        URL.<br />Please update your control panel manually to disable remote
        authentication.</div>'),
     'error', FALSE);
    return;
  }

  if ($qra['result'] == 0) {
    drupal_set_message(
      t('<div style="margin-left: 12px;">Error disabling
        remote authentication URL: <b>' . filter_xss($qra['message']) .
        '</b></div>'),
      'error', FALSE
     );
    return;
  }

  drupal_set_message(t('Remote authentication disabled.'));
}

/**
 * Set Remote Authentication Script
 */
function _addonchat_enable_remote_authentication($aid, $md5pw, $rasurl) {
  $remote_link = "http://clientx.addonchat.com/setras.php?id=" . intval($aid) .
    "&md5pw=" . urlencode($md5pw) .
    "&rasurl=" . urlencode($rasurl);

  if (($raw_result = _addonchat_geturl($remote_link)) === FALSE) {
    return FALSE;
  }

  $qra = array();
  list($qra['result'], $qra['message']) = $raw_result;

  return $qra;
}


/**
 * Unset Remote Authentication Script
 */
function _addonchat_disable_remote_authentication($aid, $md5pw) {
  $remote_link = "http://clientx.addonchat.com/setras.php?unset=1&id=" .
    intval($aid) . "&md5pw=" . urlencode($md5pw);

  if (($raw_result = _addonchat_geturl($remote_link)) === FALSE) {
    return FALSE;
  }

  $qra = array();
  list($qra['result'], $qra['message']) = $raw_result;

  return $qra;
}


/**
 * Fetch AddonChat Account Details
 */
function _addonchat_fetch_account($aid, $pw) {
  $remote_link = "http://clientx.addonchat.com/queryaccount.php?id=" .
    intval($aid) . "&md5pw=" . urlencode($pw);

  if (($raw_result = _addonchat_geturl($remote_link)) === FALSE) {
    return FALSE;
  }

  $qra = array();
  list(
    $qra['edition'],
    $qra['modules'],
    $qra['ras_capable'],
    $qra['edition_name'],
    $qra['expire_date'],
    $qra['ras_enabled'],
    $qra_IGNORED['ras_url'],
    $qra['server_name'],
    $qra['server_port'],
    $qra['cpanel_login'],
    $qra['title'],
    $qra['account_id_fq'],
    $qra['customer_id_fq']
  ) = $raw_result;

  return $qra;
}

/**
 * Fetch Web Address (file, CURL)
 */
function _addonchat_geturl($rlink) {
  $rlink = trim($rlink);

  if (ini_get('allow_url_fopen') == 0) {
    if (!function_exists('curl_init')) {
      return FALSE;
    }

    else {
      $ch = curl_init($rlink);
      curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
      curl_setopt($ch, CURLOPT_TIMEOUT_MS, 5000);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 5000);

      if (($result = curl_exec($ch)) === FALSE) {
        return FALSE;
      }

      curl_close($ch);

      $lines = split("\n", $result);

      // Lets ditch the empty lines here...
      $new_lines = array();
      foreach ($lines as $linecheck) {
        if (trim($linecheck) == "") {
        }
        else {
          $new_lines[] = $linecheck . "\n";
        }
      }

      return $new_lines;
    }
  }
  else {
    return file($rlink);
  }
}

/**
 * Construct Default Remote Authentication URL
 */
function _addonchat_default_rasurl() {
  global $base_url;

  $rasurl = trim($base_url);
  if (!(drupal_substr($rasurl, -1) == '/')) {
    $rasurl .= '/';
  }
  $rasurl .= 'addonchat_auth.php';

  return $rasurl;
}
