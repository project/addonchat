$Id

   ============
   INSTALLATION
   ============

      Please read the included INSTALL.txt for installation and set up
      instructions.

   ============
   REQUIREMENTS
   ============

      A. DRUPAL 6 (http://www.drupal.org)

      B. Any AddonChat account (http://www.addonchat.com)

   =======
   SUPPORT
   =======

      AddonInteractive provides full support and installation assistance for
      this Drupal module. Questions, comments, or suggestions may be directed
      to support@addonchat.com or online at http://support.addoninteractive.com
